import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit, OnChanges, OnDestroy {
  // items: number[] = [1, 4, 3, 56, 3, 4];
  @Input() counter = 0;
  title = 'Fazer compras';
  @Input() avisar: number;
  @Input() mostrarContador: boolean;

  @Input() indice: number;
  @Input() item: number;
  @Output() deleteItem = new EventEmitter<number>();

  constructor() { }

  onButtonClick() {
    this.title = 'MUDOU';
  }

  onAddButtonClick() {
    this.counter++;
  }
  onSubButtonClick() {
    this.counter--;
  }

  onLoginButtonClick(numero: number) {
    console.log(numero);
  }

  onDeleteButtonClick(i: number) {
    // this.items.splice(i, 1);
    this.deleteItem.emit(i);
  }

  ngOnDestroy(): void {
    console.log(`o item ${this.item} saiu da tela`);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.indice) {
      console.log(`o indice do item ${this.item} era ${changes.indice.previousValue} e agora é ${changes.indice.currentValue}`);

    }
    // console.log(changes);
  }

  ngOnInit() {
    /* setInterval(() => {
      this.counter++;
    }, 1000); */

    /* setInterval(() => {
      this.title = 'Compras efetuadas';
    }, 2000); */
  }

}
