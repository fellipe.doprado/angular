import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';
  itemApp = 'Maça';
  items: number[] = [1, 3, 4];

  constructor() {
    setTimeout(() => {
      this.itemApp = 'Banana';
    }, 2000);
  }

  onDeleteItem(item: number) {
    this.items.splice(item, 1);
  }
}
