import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email = '';
  password = '';
  password_confirm = '';

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  signUp() {
    console.log(this.email, this.password, this.password_confirm);
    if (this.password !== this.password_confirm) {
      alert ('Senhas diferentes');
      return;
    }

    this.authService.signup(this.email, this.password).subscribe((value) => {
      console.log(value);
    }, error => {
      console.log(error);
      console.log(error.error);

      switch (error.error.error.message) {
        case 'EMAIL_EXISTS':
          alert('E-mail já existente');
        break;
        case 'TOO_MANY_ATTEMPTS_TRY_LATER':
          alert('Muitas tentativas, tente novamente mais tarde');
        break;
        case 'WEAK_PASSWORD':
          alert('A senha deve ter no mínimo 6 caracteres');
        break;
        default:
          alert('Houve um erro');
        break;
      }
    });

  }

}
