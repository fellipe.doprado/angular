import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthState } from './store/reducers/auth.reducer';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.store.select('auth').pipe(
        map(value => {
          if (value.user == null) {
            this.router.navigateByUrl('/');
            return false;
          }

          return true;
        })
      );
  }

  constructor (
    private store: Store<AuthState>,
    private router: Router
  ) {

  }
}
