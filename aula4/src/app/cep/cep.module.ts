import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CepDetailsComponent } from './cep-details/cep-details.component';
import { CepExibicaoComponent } from './cep-exibicao/cep-exibicao.component';
import { CepBuscaComponent } from './cep-busca/cep-busca.component';
import { CommonModule } from '@angular/common';

const appRoutes: Routes = [
  // { path: 'cep/:cep', component: CepDetailsComponent},
  // { path: 'cep', component: CepBuscaComponent},
  { path: ':cep', component: CepDetailsComponent},
  { path: '', component: CepBuscaComponent},

];

@NgModule({
  declarations: [
    CepDetailsComponent,
    CepExibicaoComponent,
    CepBuscaComponent,
  ],
  imports: [
    RouterModule.forChild(appRoutes),
    FormsModule,
    CommonModule
  ]
})

export class CepModule {

}
