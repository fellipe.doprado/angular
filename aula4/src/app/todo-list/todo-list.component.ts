import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TodoService } from './../todo.service';
import { Component, OnInit } from '@angular/core';
import { AuthState } from '../store/reducers/auth.reducer';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todos$: Observable<Todo[]>;

  constructor(
    private todoService: TodoService,
    private store: Store<AuthState>,
    private router: Router
  ) {
    store.select('auth').subscribe(value => {
      this.todos$ = todoService.getTodos(value.user.localId);
    });
    /* todoService.getTodos().subscribe((todos) => {
      this.todos = todos;
      console.log(todos);
    });*/
  }

  ngOnInit() {
  }

  onEditToDo(id: number) {
    this.router.navigateByUrl(`todos/${id}/edit`);
  }

  onDeleteToDo(id: number) {
    if (!confirm('Deseja apagar mesmo esse ToDo?')) {
      return;
    }

    this.todoService.deleteTodo(id).subscribe(() => {
      this.todos$ = this.todos$.pipe(map(todos => {
        return todos.filter(t => t.id !== id);
      }));

      alert('ToDo deletado com sucesso');
    });

    this.router.navigateByUrl('/todos');
  }

}
