import { AuthState } from './../store/reducers/auth.reducer';
import { Store } from '@ngrx/store';
import { TodoService } from './../../../../aula4/src/app/todo.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  todo: Todo = {
    id: 0,
    title: '',
    description: '',
    finished: false,
    date: '',
    userId: '',
  };

  isAdding = true;

  constructor(
    private todoService: TodoService,
    private router: Router,
    private state: Store<AuthState>,
    private activatedRoute: ActivatedRoute
  ) {
    state.select('auth').subscribe(value => {
      if (value.user != null) {
        this.todo.userId = value.user.localId;
      }
    });

    activatedRoute.params.subscribe((params) => {
      if (params.id) {
        this.isAdding = false;
        todoService.getTodo(params.id).subscribe(todo => {
          this.todo = todo;
        });
      }
    });
  }

  ngOnInit() {
  }

  onFormSend() {
    console.log(this.todo);
    if (this.isAdding) {
      this.todoService.addTodo(this.todo).subscribe(valor => {
        console.log(valor);
        alert('To Do adicionado com sucesso');
        this.router.navigateByUrl('/todos');
      }, error => {
        alert('Erro ao adicionar');
      });
    } else {
      this.todoService.updateTodo(this.todo)
      .subscribe(valor => {
        console.log(valor);
        alert('To-Do atualizado com sucesso');
        this.router.navigateByUrl('/todos');
      }, error => {
        alert('Erro ao atualizar');
      });
    }

  }

}
