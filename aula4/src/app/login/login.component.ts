import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';

  constructor(
    private authService: AuthService,
    private store: Store<AuthService>
  ) {
    store.select('auth').subscribe((value) => {
      console.log(value);
    })
  }

  ngOnInit() {
  }

  login() {
    console.log(this.email, this.password);
    this.authService.login(this.email, this.password).subscribe((value: any) => {
      this.store.dispatch({
        type: 'SET_USER',
        payload: {
          token: value.idToken,
          user: {
            email: value.email,
            localId: value.localId
          }
        }
      });
      localStorage.setItem('userToken', value.idToken);
      console.log(value);
      alert('Login efetuado com sucesso');
    }, error => {
      console.log(error);
      console.log(error.error);

      switch (error.error.error.message) {
        case 'INVALID_PASSWORD':
          alert('Senha inválida');
        break;
        case 'EMAIL_NOT_FOUND':
          alert('E-mail inválido');
        break;
        case 'USER_DISABLED':
          alert('E-mail inválido');
        break;
        default:
          alert('Houve um erro');
        break;
      }
    });
  }
}
