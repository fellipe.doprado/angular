import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private SIGNUP_URL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyChwnkBncuwCAQqDjgjY3H7ocYfO2IPnCA';
  private LOGIN_URL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyChwnkBncuwCAQqDjgjY3H7ocYfO2IPnCA';
  private ACCOUNT_INFO = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo?key=AIzaSyChwnkBncuwCAQqDjgjY3H7ocYfO2IPnCA';

  constructor(
    private http: HttpClient
  ) { }

  signup(email: string, password: string) {
    return this.http.post(this.SIGNUP_URL, {
      email: email,
      password: password
    });
  }

  login(email: string, password: string) {
    return this.http.post(this.LOGIN_URL, {
      email: email,
      password: password
    });
  }

  checkToken(idToken: string) {
    return this.http.post(this.ACCOUNT_INFO, {
      idToken: idToken,
    });
  }

}
