import { BoolPipe } from './bool.pipe';

describe('BoolPipe', () => {
  it('create an instance', () => {
    const pipe = new BoolPipe();
    expect(pipe).toBeTruthy();
  });

  it('true deve ser Sim', () => {
    const pipe = new BoolPipe();
    expect(pipe.transform(true)).toBe('Sim');
  });

  it('false deve ser Não', () => {
    const pipe = new BoolPipe();
    expect(pipe.transform(false)).toBe('Não');
  });
});
