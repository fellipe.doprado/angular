import { TestBed } from '@angular/core/testing';

import { OperadoresService } from './operadores.service';

describe('OperadoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperadoresService = TestBed.get(OperadoresService);
    expect(service).toBeTruthy();
  });

  it('deve ser 3 ao adicionar 1 + 2', () => {
    const service: OperadoresService = TestBed.get(OperadoresService);
    expect(service.add(1, 2)).toBe(3);
  });

  it('nao deve ser 4 ao adicionar 1 + 2', () => {
    const service: OperadoresService = TestBed.get(OperadoresService);
    expect(service.add(1, 2)).not.toBe(4);
  });

  it('deve menor ou igual 3 ao adicionar 1 + 2', () => {
    const service: OperadoresService = TestBed.get(OperadoresService);
    expect(service.add(1, 2)).toBeLessThanOrEqual(3);
    expect(service.add(1, 2)).toBe(3);
  });
});
