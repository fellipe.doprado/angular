import { Observable } from 'rxjs';
import { TodoService } from './../todo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todos$: Observable<Todo[]>;

  constructor(
    private todoService: TodoService
  ) {
    this.todos$ = todoService.getTodos();
    /* todoService.getTodos().subscribe((todos) => {
      this.todos = todos;
      console.log(todos);
    });*/
  }

  ngOnInit() {
  }

}
