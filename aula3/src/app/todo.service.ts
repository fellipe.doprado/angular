import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private URL = 'http://localhost:3000/todos/';

  constructor(
    private http: HttpClient
  ) { }

  getTodos (): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.URL);
  }

  addTodo (todo: Todo) {
    return this.http.post(this.URL, todo);
  }
}
