import { TodoService } from './../../../../aula3/src/app/todo.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  todo: Todo = {
    id: 0,
    title: '',
    description: '',
    finished: false,
    date: '',
  }

  constructor(
    private todoService: TodoService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onFormSend() {
    console.log(this.todo);
    this.todoService.addTodo(this.todo).subscribe(valor => {
      console.log(valor);
      alert('To Do adicionado com sucesso');
      this.router.navigateByUrl('/todos');
    }, error => {
      alert('Erro ao adicionar');
    });
  }

}
