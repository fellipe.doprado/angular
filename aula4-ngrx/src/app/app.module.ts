import { messagesReducer } from './store/reducers/messages.reducer';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { HeaderComponent } from './header/header.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    StoreModule.forRoot({
      messages: messagesReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Exibir apenas os últimos 25 estados
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
