import { AddMessageAction, DeleteMessageAction } from './../store/actions/message.action';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { MessageState } from '../store/reducers/messages.reducer';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  messageText = '';

  // messagesList = [];
  messagesList$: Observable<any>;

  constructor(
    private store: Store<MessageState>
  ) {
    this.messagesList$ = store.select('messages').pipe(pluck('messagesList'));
    console.log(this.messagesList$);
    /* store.select('messages').subscribe(value => {
      this.messagesList = value.messagesList;
      console.log(value);
    }); */
  }

  ngOnInit() {
  }

  onSendClick() {
    // console.log(this.messageText);
    /* this.store.dispatch({
      type: 'ADD_MESSAGE',
      payload: {
        text: this.messageText,
        date: new Date(),
      }
    }); */
    this.store.dispatch(new AddMessageAction({
      text: this.messageText,
      date: new Date(),
    }));
    this.messageText = '';
  }

  onDeleteClick(i: number) {
    /* this.store.dispatch({
      type: 'DELETE_MESSAGE',
      payload: {
        indice: i,
      }
    }); */
    this.store.dispatch(new DeleteMessageAction({
      indice: i,
    }));
  }

}
