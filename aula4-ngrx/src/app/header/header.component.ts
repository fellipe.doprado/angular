import { MessageState } from './../store/reducers/messages.reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // messageCount = 0;
  messageCount$: Observable<number>;

  constructor(
    private store: Store<MessageState>
  ) {
    this.messageCount$ = store.select('messages').pipe(pluck('messagesList', 'length'));
    /* store.select('messages').subscribe(value => {
      this.messageCount = value.messagesList.length;
    }); */
  }

  ngOnInit() {
  }

}
